Source: libweb-id-perl
Section: perl
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 libcrypt-x509-perl <!nocheck>,
 libdatetime-perl <!nocheck>,
 libexporter-tiny-perl <!nocheck>,
 liblist-moreutils-perl <!nocheck>,
 libtype-tiny-perl <!nocheck>,
 libpath-tiny-perl <!nocheck>,
 libplack-perl <!nocheck>,
 librdf-query-perl <!nocheck>,
 librdf-trine-perl <!nocheck>,
 libfile-sharedir-perl <!nocheck>,
 libtypes-datetime-perl <!nocheck>,
 libtypes-uri-perl <!nocheck>,
 liburi-perl <!nocheck>,
 libnamespace-sweep-perl <!nocheck>,
 libmoose-perl <!nocheck>,
 libmatch-simple-perl <!nocheck>,
 openssl <!nocheck>,
 libcapture-tiny-perl <!nocheck>,
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libweb-id-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libweb-id-perl
Homepage: https://metacpan.org/release/Web-ID
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no

Package: libweb-id-perl
Architecture: all
Depends:
 libcrypt-x509-perl,
 libdatetime-perl,
 libexporter-tiny-perl,
 libfile-sharedir-perl,
 liblist-moreutils-perl,
 libmatch-simple-perl,
 libmoose-perl,
 libnamespace-sweep-perl,
 libpath-tiny-perl,
 libplack-perl,
 librdf-query-perl,
 librdf-trine-perl,
 libtypes-datetime-perl,
 libtypes-uri-perl,
 libtype-tiny-perl,
 liburi-perl,
 ${misc:Depends},
 ${perl:Depends}
Recommends:
 openssl,
Suggests:
 libwww-finger-perl,
 libplack-middleware-apache2-modssl-perl,
 libplack-middleware-gepokx-modssl-perl,
Description: implementation of WebID (a.k.a. FOAF+SSL)
 WebID is a simple authentication protocol
 based on TLS (Transaction Layer Security,
 better known as Secure Socket Layer, SSL)
 and the Semantic Web.
 This module provides a Perl implementation
 for authenticating clients using WebID.
 .
 For more information see the Web::ID::FAQ document.
 .
 Bundled with this module are Plack::Middleware::Auth::WebID,
 a plugin for Plack
 to perform WebID authentication on HTTPS connections;
 and Web::ID::Certificate::Generator,
 a module that allows you to generate WebID-enabled certificates
 that can be installed into web browsers.
 .
 If WWW::Finger (libwww-finger-perl) is installed,
 Web::ID::Certificate::Generator attempts to locate some RDF data
 about the holder of an e-mail address
 provided as Web::ID::SAN::Email.
 It is probably not especially interoperable
 with other WebID implementations.
 .
 Depending on webserver setup,
 libplack-middleware-apache2-modssl-perl
 or libplack-middleware-gepokx-modssl-perl
 may be required.
